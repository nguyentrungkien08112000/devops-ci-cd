#!/bin/bash
OUTPUT=$(curl -s -X GET localhost:1000/green)
if [[ "$OUTPUT" == *"$DESIRED_OUTPUT_CONTAINS"* ]]; then
  echo "Test success with output: $OUTPUT"
  echo "Success !"
  exit 0
else
  echo "Failed !"
  exit 1
fi